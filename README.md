# README

### Opis funkcjonalności

Walidator numeru pesel, po uruchomieniu programu wyświetla się okno, w którym należy podać numer pesel do sprawdzenia.
Po naciśnięciu przycisku sprawdź, otrzymamy następujące informacje:
* podany pesel
* płeć
* komunikat o poprawnej liczbę kontrolnej
* dzień/miesiąc/rok
* data w formacie dd-mm-yyyy.

Jeżeli podamy pesel, który nie składa się z 11 cyfr lub błędną liczbę kontrolną nie otrzymamy żadnych informacji.

### Informacje techniczne

Aplikacja napisana w oparciu o język JAVA przy użyciu IntelliJ IDEA z prostym interfejsem JavaFx, posiadająca 2 sceny.

### Wygląd interfejsu graficznego

![Scena1](images/scene1.PNG)
![Scena2](images/scene2.PNG)


### Podział prac

Jewiarz Jakub              | Kijowski Bartłomiej
-------------              | -------------
Obsługa interfejsu graficznego   | Wygląd interfejsu
Wybrane funkcjonalności programu | Wybrane funkcjonalności programu