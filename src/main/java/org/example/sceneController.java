package org.example;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.io.IOException;

public class sceneController {
    @FXML
    TextField nameTextField;
    @FXML
    Label ef;

    private Stage stage;
    private Scene scene;
    private Parent root;

    public void login(ActionEvent event) throws IOException {

        String pesel = nameTextField.getText();
        if (pesel.length() != 11) {
            ef.setText("Pesel musi posiadać 11 cyfr!");
        } else {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("scene2.fxml"));
            root = loader.load();
            Scene2controller pe = loader.getController();

            int[] pesel_table = new int[11];
            String[] pesel_string_tab = pesel.split(""); //zmiana całego stringa wczytanego z klawiatury na tablicę stringów, gdzie każdy element to jedna cyfra
            for (int i = 0; i < pesel_string_tab.length; i++) {
                pesel_table[i] = Integer.parseInt(pesel_string_tab[i]); //sparsowanie stringów na inty i wpisanie ich do tablicy pesel
            }

            if (pe.control_number(pesel_table) != pesel_table[10]) {
                ef.setText("Bledna liczba kontrolna! - Pesel nie istnieje");
            } else {
                pe.display_pesel(pesel);
                pe.male_or_female(pesel_table);
                pe.show_date(pesel_table);

                stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
                scene = new Scene(root);
                stage.setScene(scene);
                stage.show();
            }
        }
    }
}
