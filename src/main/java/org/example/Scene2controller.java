package org.example;

import javafx.fxml.FXML;
import javafx.scene.control.Label;

public class Scene2controller {
    @FXML
    Label nameLable;
    @FXML
    Label sex;
    @FXML
    Label control;
    @FXML
    Label dField;
    @FXML
    Label mField;
    @FXML
    Label yField;
    @FXML
    Label dateField;


   public void display_pesel(String p) {
        nameLable.setText(p);
        control.setText("Liczba kontrolna poprawna.");
    }
    public void male_or_female(int pesel[]){
        if(pesel[9]%2 == 0){
            sex.setText("Kobieta");
        }
        else{
            sex.setText("Mężczyzna");
        }
    }
    public int control_number(int pesel[]){
        int tmp = 0;
        int c_number = 10;
        int[] tab = new int[] {1,3,7,9,1,3,7,9,1,3};
        for(int i = 0; i < 10; i++){
            tab[i] = tab[i]*pesel[i];
            if (tab[i] > 9){
                tab[i] = tab[i]%10;
            }
            tmp = tmp + tab[i];
        }
        tmp = tmp%10;
        c_number = c_number - tmp;
        return c_number;
    }
    public void show_date(int pesel[]){
        int month;
        int day;
        int year;

        day = (10*pesel[4]) + pesel[5];
        month = (10*pesel[2]) + pesel[3];
        year = (10*pesel[0]) + pesel[1];

        if (month > 80 && month < 93) {
            year += 1800;
        }
        else if (month > 0 && month < 13) {
            year += 1900;
        }
        else if (month > 20 && month < 33) {
            year += 2000;
        }
        else if (month > 40 && month < 53) {
            year += 2100;
        }
        else if (month > 60 && month < 73) {
            year += 2200;
        }

        if (month > 80 && month < 93) {
            month -= 80;
        }
        else if (month > 20 && month < 33) {
            month -= 20;
        }
        else if (month > 40 && month < 53) {
            month -= 40;
        }
        else if (month > 60 && month < 73) {
            month -= 60;
        }

        dField.setText("Dzień: " + day);
        mField.setText("Miesiąc: " + month);
        yField.setText("Rok: " + year);

        String string_day = Integer.toString(day);
        String string_month = Integer.toString(month);

        if(day < 10 && month < 10){
            dateField.setText("Data urodzenia: " + "0" + string_day + "-" + "0" + string_month + "-" + year);
        }
        else if(day < 10 && month > 9){
            dateField.setText("Data urodzenia: " + "0" + string_day + "-" + string_month + "-" + year);
        }
        else if(day > 9 && month < 10){
            dateField.setText("Data urodzenia: " + string_day + "-" + "0" + string_month + "-" + year);
        }
        else{
            dateField.setText("Data urodzenia: " + string_day + "-" + string_month + "-" + year);
        }
   }
}
